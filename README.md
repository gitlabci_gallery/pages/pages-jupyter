This gitlab project gives an example of generating an html documentation from a few jupyter notebooks.

# The jupyter notebooks

This project includes three jupyter notebooks which are three small tutorials on the Fourier transform, the finite 
elements method and the classical numerical methods that can be used to solve ordinary differential equations.

It uses the `poetry` python packaging and dependency manager. To install it see the [poetry documentation page](https://python-poetry.org/docs/).

With `poetry`, the jupyter notebooks can be played with:

```bash
poetry install --no-root
poetry run jupyter-notebook
```

# Continuous integration

In the CI process we put two stages: build and deploy. At the first stage the notebooks are played with poetry
and convert in html files. Then a small shell script build an index.html file with a list of the created html files.
At the second stage all html files are copied in the page `public` folder.

A `.pre` stage is used to build the docker image that will be kept in the registry (it is done by the included template
[register-dockerfile.yml](https://gitlab.inria.fr/inria-ci/docker/register-dockerfile/register-dockerfile.yml)). 
One docker image is built per branch and a final clean job is used to remove the old ones.

In the docker image texlive is installed as it is needed to build the jupyter-notebook. Since texlive can be 
time-consuming to install, it is relevant not to rebuild the docker image after each commit. That is why an image is 
built only for a new branch or if the Dockerfile has changed. 

Before launching the generation be sure you activated the CI/CD option in the 
`Settings/General/Visibility, project features, permissions` section of your
gitlab project. You have also to enable shared runners for the project in the `Settings/CI/CD/Runners` section.

To see the notebooks results open the `Settings/Pages` section, you will find the url.

That's it!

# Notes

In the `Finite_Element` page, we can see that the svg images became dark in the html view. This is a known bug of 
jupyter nbconvert reported by the following issue: https://github.com/jupyter/nbconvert/issues/1863.

# Mybinder (`mybinder` branch)

To deploy the project on [mybinder](https://mybinder.org/) we added a binder repository with three files:
* `apt.txt` specify the debian package we want to install in the docker image.
* `requirements.txt` specifies the python modules we want to install globally. Basically we just need poetry.
* `postBuild` contains instructions that will be run from the shell after the environment has been installed. 
In our case we run the poetry install command to install our project dependencies.

We also have to add a `poetry.toml` to tell poetry not to create an new virtual environment but to use the one that is 
deployed by binder. If we do not include this file, the command "poetry install" will not be installed in the virtual
environment of binder. But as we do not want to have this file activated elsewhere we create it in the 
`postBuild`file just before calling the `poetry install` command.

We build the docker environment on mybinder.org and you can access it here : 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fgitlabci_gallery%2Fpages%2Fpages-jupyter.git/mybinder)
